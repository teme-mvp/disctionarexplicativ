﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        public SearchWindow()
        {
            InitializeComponent();
        }

        private void By_Word_Button_Click(object sender, RoutedEventArgs e)
        {
            SearchByWordWindow byWordWindow = new SearchByWordWindow();
            byWordWindow.Show();
            this.Close();
        }

        private void By_Category_Button_Click(object sender, RoutedEventArgs e)
        {
            SearchByCategoryWindow byCategoryWindow = new SearchByCategoryWindow();
            byCategoryWindow.Show();
            this.Close();
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
