﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for QuestionWindow.xaml
    /// </summary>
    public partial class QuestionWindow : Window
    {
        private Game game;
        private List<int> chosenWords;
        public QuestionWindow(int currentWord, int correctNr, List<int> alreadyChosen)
        {
            InitializeComponent();
            txtCurrentNo.Text = currentWord.ToString();
            txtCorrectNo.Text = correctNr.ToString();
            if (currentWord == 5)
                button.Content = "Finish";
            else
                button.Content = "Next";

            game = new Game();
            chosenWords = new List<int>();
            chosenWords = alreadyChosen;

            game.getRandomWordAndDescription(alreadyChosen);

            txtDescription.Text = game.wordDescription;
        }

        private void Next_Button_Click(object sender, RoutedEventArgs e)
        {
            if (button.Content == "Finish")
            {
                int nrCorecte = verifyAnswer();

                ResultsWindow resultsWindow = new ResultsWindow(nrCorecte);
                resultsWindow.Show();
                this.Close();
            }
            else
            {
                verifyAnswer();
            }
        }

        private int verifyAnswer()
        {
            int correctNo = Int32.Parse(txtCorrectNo.Text);

            if (button.Content == "Next")
            {
                if (answerBox.Text != "")
                {
                    if (answerBox.Text.ToUpper() == game.chosenWord.ToUpper())
                    {
                        correctNo++;
                        MessageBox.Show("Corect!");
                    }
                    else
                    {
                        MessageBox.Show("Gresit! Cuvantul corect era: " + game.chosenWord);
                    }

                    QuestionWindow gameWindow = new QuestionWindow(Int32.Parse(txtCurrentNo.Text) + 1, correctNo, chosenWords);
                    gameWindow.Show();
                    this.Close();
                }
                else
                    MessageBox.Show("Nu ati introdus niciun raspuns!");
            }
            else
            {
                if (answerBox.Text != "")
                {
                    if (answerBox.Text == game.chosenWord)
                    {
                        correctNo++;
                        MessageBox.Show("Corect!");
                    }
                    else
                    {
                        MessageBox.Show("Gresit! Cuvantul corect era: " + game.chosenWord);
                    }
                }
            }
            return correctNo;
        }
    }
}
