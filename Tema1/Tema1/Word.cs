﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema1
{
    class Word
    {
        public Word(string name, string category, string description)
        {
            Name = name;
            Category = category;
            Description = description;
        }

        public Word() { }

        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }

        public void addCategoryToFile()
        {
            string category_file_name = "category.txt";

            List<string> allCategories = new List<string>();
            allCategories = File.ReadAllLines(category_file_name).ToList();

            bool found = false;

            foreach (string category in allCategories)
            {
                if (category == Category)
                    found = true;
            }

            if (!found)
            {
                allCategories.Add(Category);
                File.WriteAllLines(category_file_name, allCategories);
            }
        }

        public void addWordToFile()
        {
            string words_file_name = "words.txt";

            List<string> allWords = new List<string>();
            allWords = File.ReadAllLines(words_file_name).ToList();

            bool found = false;

            foreach (string word in allWords)
            {
                if (word == Name)
                    found = true;
            }

            if (!found)
            {
                allWords.Add(Name);
                File.WriteAllLines(words_file_name, allWords);

                string dictionary_file_name = "dictionary.txt";

                TextWriter textWriter = new StreamWriter(dictionary_file_name, true);

                textWriter.WriteLine(Category + '~' + Name + '~' + Description);
                textWriter.Flush();
            }
            else
            {
                System.Windows.MessageBox.Show("Cuvantul exista deja in dictionar!");
            }
        }
    }
}

