﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for SearchByWordWindow.xaml
    /// </summary>
    public partial class SearchByWordWindow : Window
    {
        private Dictionary dictionary;
        public SearchByWordWindow()
        {
            InitializeComponent();
            dictionary = new Dictionary();
            loadWords();
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
           SearchWindow searchWindow = new SearchWindow();
           searchWindow.Show();
           this.Close();
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            if (combo.Text != "" || combo.SelectedItem != null)
                if (combo.Text != "")
                    dictionary.searchWord(combo.Text);
                else
                {
                    if (combo.SelectedItem != null)
                        dictionary.searchWord((string)combo.SelectedItem);
                }
            else
                MessageBox.Show("Nu ati introdus niciun cuvant!");
        }

        private void loadWords()
        {
            string words_file_name = "words.txt";
            List<string> allWords = new List<string>();
            allWords = File.ReadAllLines(words_file_name).ToList();

            foreach (string word in allWords)
            {
                combo.Items.Add(word);
            }
        }
    }
}
