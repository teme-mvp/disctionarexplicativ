﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for DeleteWindow.xaml
    /// </summary>
    public partial class DeleteWindow : Window
    {
        Dictionary dictionary;
        public DeleteWindow()
        {
            InitializeComponent();
            dictionary = new Dictionary();
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtWord.Text != "")
            {
                if(dictionary.deleteWordFromFile(txtWord.Text, "dictionary.txt", 2))
                    MessageBox.Show("Deleted word: " + txtWord.Text);
            }
            else
            {
                MessageBox.Show("Nu ati introdus niciun cuvant!");
            }
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow adminWindow = new AdminWindow();
            adminWindow.Show();
            this.Close();
        }
    }
}
