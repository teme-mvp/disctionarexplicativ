﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema1
{
    class Game
    {
        public Game()
        {
            dictionary = new Dictionary();
        }

        public Dictionary dictionary { get; set; }
        public string chosenWord { get; set; }
        public string wordDescription { get; set; }

        public void getRandomWordAndDescription(List<int> alreadyChosen)
        {
            Word word = new Word();
            word = dictionary.getRandomWord(alreadyChosen);
            chosenWord = word.Name;
            wordDescription = word.Description;
        }
    }
}
