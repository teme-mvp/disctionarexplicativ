﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>
    public partial class AddWindow : Window
    {
        Dictionary dictionary;
        public AddWindow()
        {
            InitializeComponent();
            dictionary = new Dictionary();
            loadCategories();
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            Word word = new Word(txtWord.Text, comboCategory.Text, txtDescription.Text);
            if (comboCategory.SelectedItem != null)
            {
               word.Category = (string) comboCategory.SelectedItem;
            }
            if (comboCategory.SelectedItem == null && comboCategory.Text == "")
            {
                MessageBox.Show("Trebuie sa alegeti o categorie sau sa introduceti una noua.");
            }
            else
            {
                if (comboCategory.Text != "")
                    word.Category = comboCategory.Text;
                dictionary.addWordToDictionary(word);
                MessageBox.Show("Added word: " + txtWord.Text);
            }
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow adminWindow = new AdminWindow();
            adminWindow.Show();
            this.Close();
        }

        private void loadCategories()
        {
            string category_file_name = "category.txt";
            List<string> allCategories = new List<string>();
            allCategories = File.ReadAllLines(category_file_name).ToList();

            foreach (string category in allCategories)
            {
                comboCategory.Items.Add(category);
            }
        }
    }
}
