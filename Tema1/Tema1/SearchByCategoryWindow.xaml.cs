﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for SearchByCategoryWindow.xaml
    /// </summary>
    public partial class SearchByCategoryWindow : Window
    {
        private Dictionary dictionary;
        public SearchByCategoryWindow()
        {
            InitializeComponent();
            dictionary = new Dictionary();
            loadCategories();
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            SearchWindow searchWindow = new SearchWindow();
            searchWindow.Show();
            this.Close();
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            if (comboWords.SelectedItem != null)
                dictionary.searchWord((string)comboWords.SelectedItem);
            else
                MessageBox.Show("Nu ati selectat niciun cuvant!");
        }

        private void loadCategories()
        {
            string category_file_name = "category.txt";
            List<string> allCategories = new List<string>();
            allCategories = File.ReadAllLines(category_file_name).ToList();

            foreach (string category in allCategories)
            {
                comboCategory.Items.Add(category);
            }
        }

        private void combo_CategorySelected(object sender, SelectionChangedEventArgs e)
        {
            comboWords.Items.Clear();
            string[] lines = File.ReadAllLines("dictionary.txt");

            for (int index = 0; index < lines.Length; index++)
            {
                string[] fields = lines[index].Split('~');
                if (fields[0] == (string)comboCategory.SelectedItem)
                {
                    comboWords.Items.Add(fields[1]);
                }
            }
        }
    }
}
