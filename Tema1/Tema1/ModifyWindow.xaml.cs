﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for ModifyWindow.xaml
    /// </summary>
    public partial class ModifyWindow : Window
    {
        Dictionary dictionary;

        public ModifyWindow()
        {
            InitializeComponent();
            dictionary = new Dictionary();
        }

        private void Modify_Button_Click(object sender, RoutedEventArgs e)
        {
            if ((txtWord.Text == "") || (txtNewCategory.Text == "") || (txtNewWord.Text == "") || (txtNewDescription.Text == ""))
                MessageBox.Show("Completati toate casutele inainte de a apasa butonul de modificare!");
            else
            {
                if(dictionary.modifyWordFromFile(txtWord.Text, "dictionary.txt", 2, txtNewCategory.Text, txtNewWord.Text, txtNewDescription.Text))
                    MessageBox.Show("Modiffied word: " + txtWord.Text);
            }
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow adminWindow = new AdminWindow();
            adminWindow.Show();
            this.Close();
        }
    }
}
