﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tema1
{
    class Dictionary
    {
        public Dictionary()
        {
            words = new List<Word>();
        }

        public List<Word> words { get; set; }

        public void addWordToDictionary(Word word)
        {
            words.Add(word);
            word.addCategoryToFile();
            word.addWordToFile();
        }

        public bool modifyWordFromFile(string searchTerm, string filePath, int positionOfSearchTerm, string fieldOne, string fieldTwo, string fieldThree)
        {
            positionOfSearchTerm--;
            string tempFile = "temp.txt";
            bool edited = false;

            try
            {
                string[] lines = File.ReadAllLines(@filePath);

                for (int index = 0; index < lines.Length; index++)
                {
                    string[] fields = lines[index].Split('~');
                    if (!(recordMatches(searchTerm, fields, positionOfSearchTerm)))
                    {
                        addRecord(fields[0], fields[1], fields[2], @tempFile);
                    }
                    else
                    {
                        if (!edited)
                        {
                            addRecord(fieldOne, fieldTwo, fieldThree, @tempFile);
                            edited = true;
                        }
                    }
                }

                File.Delete(@filePath);
                File.Move(tempFile, filePath);

                if (!edited)
                {
                    MessageBox.Show("Cuvantul introdus nu exista in dictionar.");
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error: ", ex);
            }
        }

        public bool deleteWordFromFile(string searchTerm, string filePath, int positionOfSearchTerm)
        {
            positionOfSearchTerm--;
            string tempFile = "temp.txt";
            bool found = false;

            try
            {
                string[] lines = File.ReadAllLines(@filePath);

                for (int index = 0; index < lines.Length; index++)
                {
                    string[] fields = lines[index].Split('~');
                    if (!(recordMatches(searchTerm, fields, positionOfSearchTerm)))
                    {
                        addRecord(fields[0], fields[1], fields[2], @tempFile);
                    }
                    else
                    {
                        Word toDelete = new Word(fields[0], fields[1], fields[2]);
                        words.Remove(toDelete);
                        found = true;
                    }
                }

                File.Delete(@filePath);
                File.Move(tempFile, filePath);

                if(!found)
                {
                    MessageBox.Show("Cuvantul introdus nu exista in dictionar.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error: ", ex);
            }
        }

        public static bool recordMatches(string searchTerm, string[] record, int positionOfSearchTerm)
        {
            if (record[positionOfSearchTerm].Equals(searchTerm))
            {
                return true;
            }
            return false;
        }

        public static void addRecord(string category, string word, string description, string filePath)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@filePath, true))
                {
                    file.WriteLine(category + '~' + word + '~' + description);
                    file.Flush();

                    string category_file_name = "category.txt";

                    List<string> allCategories = new List<string>();
                    allCategories = File.ReadAllLines(category_file_name).ToList();

                    bool found = false;

                    foreach (string filecategory in allCategories)
                    {
                        if (filecategory == category)
                            found = true;
                    }

                    if (!found)
                    {
                        allCategories.Add(category);
                        File.WriteAllLines(category_file_name, allCategories);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Eroare: ", ex);
            }
        }

        public void searchWord(string word)
        {
            int positionOfSearchTerm = 1;
            bool found = false;
            try
            {
                string[] lines = File.ReadAllLines("dictionary.txt");

                for (int index = 0; index < lines.Length; index++)
                {
                    string[] fields = lines[index].Split('~');
                    if (recordMatches(word, fields, positionOfSearchTerm))
                    {
                        InfoWindow info = new InfoWindow(fields[0], fields[1], fields[2]);
                        info.Show();
                        found = true;
                    }
                }
                if (!found)
                    MessageBox.Show("Cuvantul cautat nu exista in dictionar sau a fost sters.");
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error: ", ex);
            }
        }

        public Word getRandomWord(List<int> alreadyChosen)
        {
            string[] lines = File.ReadAllLines("dictionary.txt");

            Random random = new Random();
            int r = random.Next(lines.Length - 1);

            while(alreadyChosen.Contains(r))
            {
                r = random.Next(lines.Length - 1);
            }

            alreadyChosen.Add(r);

            string[] fields = lines[r].Split('~');

            return new Word(fields[1], fields[0], fields[2]);
        }
    }
}
